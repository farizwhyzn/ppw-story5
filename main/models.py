from django.db import models

# Create your models here.

class Matkul(models.Model):
    matkul_name = models.CharField(max_length = 200)
    dosen_name = models.CharField(max_length = 200)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    tahun_semester = models.CharField(max_length = 200)
    ruang_kelas = models.CharField(max_length = 200)

